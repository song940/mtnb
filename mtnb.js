/**
 * [MTNB description]
 * @param {[type]} options [description]
 */
function MTNB(options){
  if(!(this instanceof MTNB))
    return new MTNB(options);
  // options
  this.options = options || {};
  return this;
};
/**
 * [function description]
 * @return {[type]} [description]
 */
MTNB.log = function(message){
  if(!MTNB.debug) return;
  message = '[MTNB] ' + JSON.stringify(message);
  if(!!~[ 'android', 'iphone' ].indexOf(MTNB.os())){
    alert(message);
  }else{
    console.debug(message);
  }
}
/**
 * [function description]
 * @param  {[type]} ua [description]
 * @return {[type]}    [description]
 */
MTNB.os = function(ua){
  ua = ua || navigator.userAgent;
  return ((ua.match(/(iPhone|Android)/i)||[])[1] || '').toLowerCase();
};
/**
 * [function description]
 * @param  {[type]} name [description]
 * @return {[type]}      [description]
 */
MTNB.cookie = function(name){
  var cookies = {};
  var cookie = document.cookie;
  cookie.replace(/(.*?)=(.*?)($|;)\s?/g, function(_, name, value){
    cookies[ name ] = value;
  });
  return name ? cookies[ name ] : cookies;
};
/**
 * [function description]
 * @param  {[type]} ua [description]
 * @return {[type]}    [description]
 */
MTNB.version = function(ua){
  ua = ua || navigator.userAgent;
  return /MTNB/.test(ua) && ua.match(/MTNB\/(\d+.\d+.\d+)/)[1];
};
/**
 * [function description]
 * @param  {[type]} namespace [description]
 * @return {[type]}           [description]
 */
MTNB.support = function(namespace){
  var a = MTNB.cookie('mtnb');
  var b = a.split(',');
  b.push('basic@core');
  for(var i in b){
    var c = b[i].split('@');
    if(!!~namespace.indexOf([ c[0], c[1] ].join('.'))){
      return true;
    }
  }
};
/**
 * [function description]
 * @param  {[type]} obj [description]
 * @return {[type]}     [description]
 */
MTNB.typeof = function(obj){
  return ({}).toString.call(obj).match(/\[object (\w+)\]/)[1].toLowerCase();
}
/**
 * [function uuid]
 * @param  {[type]} start [description]
 * @return {[type]}       [description]
 */
MTNB.uuid = function(){
  MTNB.uniqueId = MTNB.uniqueId || 0;
  return [ ++MTNB.uniqueId, +new Date ].join('_');
};
/**
 * [function registerHandler]
 * @param  {[type]} type    [description]
 * @param  {[type]} handler [description]
 * @return {[type]}         [description]
 */
MTNB.registerHandler = function(type, handler, uuid){
  uuid = [ type, MTNB.uuid() ].join('_');
  MTNB.handlers = MTNB.handlers || {};
  MTNB.handlers[ uuid ] = handler;
  return uuid;
};
/**
 * [function requestHandler]
 * @param  {[type]} handlerId [description]
 * @return {[type]}           [description]
 */
MTNB.requestHandler = function(handlerId){
  return (MTNB.handlers || {})[ handlerId ];
}
/**
 * [function _handleMessageFromApp]
 * @param  {[type]} message [description]
 * @return {[type]}         [description]
 */
MTNB._handleMessageFromApp = function(message){
  if(!message || !(MTNB.typeof(message) == 'object'))
    return MTNB.log('Unexpected message from app: ' + message);
  if(message.handlerId)  return MTNB.requestHandler(message.handlerId )(message.data);
  if(message.callbackId) return MTNB.requestHandler(message.callbackId)(message.data);
};
/**
 * [function description]
 * @param  {Function} fn       [description]
 * @param  {[type]}   params   [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
MTNB.prototype.invoke = function(fn, params, callback){
  var namespace = fn.split('.');
  if(!MTNB.version())       return MTNB.log('not support yet.');
  if(namespace.length != 3) return MTNB.log('invoke function error');
  if(!MTNB.support(fn))     return MTNB.log('method ' + fn + ' is not support yet.');
  return this.callHandler({
    businessName : namespace[0],
    moduleName   : namespace[1],
    methodName   : namespace[2],
    data         : params || {}
  }, callback);
};
/**
 * [function description]
 * @param  {[type]}   params   [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
MTNB.prototype.callHandler = function(params, callback){
  if(callback) params.callbackId = MTNB.registerHandler('cb', callback);
  function addHandlerId(obj){
    if('callback' in obj) obj.handlerId = MTNB.registerHandler('hd', obj.callback);
    return obj;
  };
  params.data = (MTNB.typeof(params.data) == 'array')
    ? params.data.map(addHandlerId)
    : addHandlerId(params.data);

  try{
    var message = JSON.stringify(params);
    switch (MTNB.os()) {
      case 'android': window.prompt(message);                     break;
      case 'iphone' : window._MTNB._handleMessageFromJs(message); break;
      default       : MTNB.log('platfrom is not supported .');    break;
    }
  }catch(e){
    MTNB.log('Caught an error on callHandler: ' + [ e.message, e.stack ].join('\n'));
  }
  return this;
};
// expose `MTNB` object for webview
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return MTNB;
  });
} else if (typeof module == 'object' && module.exports) {
  module.exports = MTNB;
} else {
  this.MTNB = MTNB;
}
