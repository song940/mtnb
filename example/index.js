const http    = require('http');
const kelp    = require('kelp');
const send    = require('kelp-send');
const body    = require('kelp-body');
const route   = require('kelp-route');
const serve   = require('kelp-static');
const logger  = require('kelp-logger');
const config  = require('kelp-config');
const cookie  = require('kelp-cookie');

/**
 * [require AuthorizationClient]
 * @param  {[type]} '../auth' [description]
 * @return {[type]}           [description]
 */
const MTNBAuth = require('../auth');
const client   = new MTNBAuth({
  domain        : "m.maoyan.com" ,
  client_id     : 'maoyan'       ,
  client_secret : 'cI4ETKb5Zvpo7laggeIjag61g0x0mwO8'
});

const app = kelp(config);

app.use(send);
app.use(body);
app.use(logger);
app.use(cookie);
app.use(serve(__dirname + '/public'));

app.use(route('/signature', function(req, res, next){
  console.log(req.cookies['mtnb']);
  client.createSignature(req.query.url).then(function(signature){
    res.send(signature);
  });
}));

// default handler
app.use(function(req, res, next){
  res.send(404);
});

http.createServer(app).listen(3000);
