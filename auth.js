const url     = require('url');
const crypto  = require('crypto');
const debug   = require('debug')('mtnb');
const request = require('superagent');
const ba      = require('@myfe/ba');

/**
 * [function AuthorizationClient]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
function AuthorizationClient(options){

  if(!(this instanceof AuthorizationClient))
    return new AuthorizationClient(options);

  this.options = Object.assign({
    auth  : 'https://mtnb.meituan.com/api/v1/auth',
    ticket: 'https://mtnb.meituan.com/api/v1/ticket'
  }, options);
};
/**
 * [function getTicket]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
AuthorizationClient.prototype.getTicket = function(callback){
  return request
  .post(this.options.ticket)
  .type('form')
  .set(ba(this.options)('post', this.options.ticket))
  .send({ domain: this.options.domain })
  .end(function(err, res){
    debug('ticket', res.body);
    callback && callback(err, (err || !res.body ||!res.body.data) ? res : res.body.data);
  });
};
/**
 * [function authorization]
 * @param  {[type]}   params   [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
AuthorizationClient.prototype.authorization = function(params, callback){
  debug('authorization', params);
  request
  .post(this.options.auth)
  .type('form')
  .send(params)
  .end(function(err, res){
    debug('authorization', res.body);
    callback && callback(err, (err || !res.body ||!res.body.data) ? res : res.body.data.result);
  });
};
/**
 * [function calculateSignature]
 * @param  {[type]} ticket   [description]
 * @param  {[type]} url      [description]
 * @param  {[type]} noncestr [description]
 * @param  {[type]} ts       [description]
 * @return {[type]}          [description]
 */
AuthorizationClient.prototype.calculateSignature = function(ticket, url, noncestr, ts){
  ts = ts || +new Date;
  noncestr = noncestr || Math.random().toString(36).substr(2, 16);
  var str = [ ticket, noncestr, ts, url ].join('');
  var signature = crypto.createHash('sha1').update(str).digest('hex').toUpperCase();
  return {
    ts      : ts,
    url     : url,
    sign    : signature,
    noncestr: noncestr
  };
};
/**
 * [function createSignature]
 * @param  {[type]} url [description]
 * @return {[type]}     [description]
 */
AuthorizationClient.prototype.createSignature = function(url){
  return new Promise((accept, reject) => {
    this.getTicket((err, ticket) => {
      if(err) return reject(err);
      accept(this.calculateSignature(ticket.ticket, url));
    });
  });
};


module.exports = AuthorizationClient;
